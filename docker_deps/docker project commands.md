# Docker Cheat Sheet

An instance of an image is called a container. You have an image, which is a set of layers as you describe, 
or a Docker Container is a Docker Image with one extra layer.  

If you start this image, you have a running container of this image. You can have many running containers 
of the same image.

So a running instance of an image is a container.

## To run a Container
```
docker run -it azhealth:symfony /bin/bash
```
OR ( specify that port 8000 should be mapped to port 80 on the host machine )
```
docker run -it azhealth:symfony -d -p 8000:80
```
OR
```
docker run -p 8000:80 azhealth/symfony:2.8
```
The container is then accessible via http://172.17.0.2/

## To Build a Docker Container from the Dockerfile
```
$ cd /path/to/Dockerfile   #e.g. cd ~/Development/astra-zeneca
```
THEN
```
$ sudo docker build .
```
OR
```
$ docker build -t astrazeneca/azhealth:latest .
```
OR
```
$ docker build -t astrazeneca/azhealth .
```

View running processes

```
$ sudo docker ps
```

View all processes

```
$ sudo docker ps -a
```

View all images
```
$ docker images
```
OR
```
$ docker image ls
```

## Various Container Deletion Strategies
Stop all containers:
```
$ docker kill $(docker ps -q)

```
Remove all containers:
```
$ docker rm $(docker ps -a -q)
```
Remove all docker images:
```
docker rmi $(docker images -q)
```

## Bidirectional File Copy

See: https://stackoverflow.com/questions/22049212/copying-files-from-docker-container-to-host?rq=1
In order to copy a file from a container to the host, you can use the command  
```
$ docker cp <containerId>:/file/path/within/container /host/path/target
```
Here's an example: (sudo is required if the user is different between host and container)
```
[jalal@goku scratch]$ sudo docker cp goofy_roentgen:/out_read.jpg .
```
Here goofy_roentgen is the name I got from the following command:
```
[jalal@goku scratch]$ sudo docker ps
[sudo] password for jalal:
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS                                            NAMES
1b4ad9311e93        bamos/openface      "/bin/bash"         33 minutes ago      Up 33 minutes       0.0.0.0:8000->8000/tcp, 0.0.0.0:9000->9000/tcp   goofy_roentgen
```

Run an image in a new container daemonized

```
$ sudo docker run -d <image_name>
```

Run an image in interactive mode with the command `/bin/bash`

```
$ sudo docker run -i -t <image_name> /bin/bash
```

Run an image in interactive mode with the command `/bin/bash` and link the ports.

```
$ sudo docker run -i -t --link <docker_container_name>:<docker_container_alias> <image_name> /bin/bash
```

Run an image with an ENTRYPOINT command in interactive mode with the command `/bin/bash`

```
$ sudo docker run --entrypoint /bin/bash -i -t <image_name>
```

Run an image in interactive mode with the command `/bin/bash` mounting the host director `/var/app/current` to the container directory `/usr/src/app`

```
$ sudo docker run -i -t -v /var/app/current:/usr/src/app/ <image_name> /bin/bash
```

Run an image in interactive mode with the command `/bin/bash` setting the environments variables `FOO` and `BAR`

```
$ sudo docker run -i -t -e FOO=foo -e BAR=bar <image_name> /bin/bash
```

# Docker Compose  
Compose is a tool for defining and running multi-container Docker applications. With Compose, you use a YAML file to configure your application’s services. Then, with a single command, you create and start all the services from your configuration.  
```
See https://docs.docker.com/compose/overview/
```

## A docker-compose.yml Looks Like This
```angular2html
version: '3'
services:
  web:
    build: .
    ports:
    - "5000:5000"
    volumes:
    - .:/code
    - logvolume01:/var/log
    links:
    - redis
  redis:
    image: redis
volumes:
  logvolume01: {}
```
