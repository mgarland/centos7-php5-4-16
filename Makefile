# call make docker_build from terminal
docker_build:
	@docker build \
	--build-arg WORK_DIR=/var/www/dev/ \
	-t docker.io/kromosome/symfony.dev .
# call make docker_push
docker_push:
	@docker push docker.io/kromosome/symfony.dev
# call make bp
bp: docker build docker_push
# call make dev
dev:
	@docker-compose down && \
        docker-compose build --pull --no-cache && \
        docker-compose \
        -f docker-compose.yml \
        up -d --remove-orphans
