FROM centos@sha256:63fa5b0e8b57e651e021799866e5f625a6235a2bdd6073f73bb96ac85b538560
ENV REFRESHED_AT 2017-01-16
# https://getcomposer.org/doc/03-cli.md#composer-allow-superuser
ENV COMPOSER_ALLOW_SUPERUSER 1
# set  server timezone: https://serverfault.com/questions/683605/docker-container-time-timezone-will-not-reflect-changes
ENV TZ=Australia/Melbourne
RUN rm /etc/localtime \
    && ln -snf /usr/share/zoneinfo/$TZ /etc/localtime \
    && echo $TZ > /etc/timezone \
    && "date"
# BUILD ARGUMENTS TO ALLOW THE WORKING DIRECTORY PATH TO BE SPECIFIED AT BUILD TIME
ARG WORK_DIR
WORKDIR $WORK_DIR
COPY app $WORK_DIR/app
COPY bin $WORK_DIR/bin
COPY src $WORK_DIR/src
COPY web $WORK_DIR/web
# COPY ACROSS ALL THE COMPOSER DEPENDENCIES TO BE INSTALLED
COPY ./docker_deps/composer.lock $WORK_DIR
COPY ./docker_deps/composer.json $WORK_DIR
# COPY ACROSS THE PARAMETERS.YML CONFIG FILE, IN PRACTICE THIS SHOULD BE HARDCODED IN PRODUCTION ENVIRON'S
# THIS WILL ALLOW US TO USE AN .env FILE TO ALTER CREDENTIALS ON A PER PROJECT BASIS, USEFUL IN DEV & CI ENVIRON'S
COPY ./app/config/parameters.yml.dist $WORK_DIR/app/config/parameters.yml
# INFORMATIVE LABELS
LABEL com.kromosome.maintainer="Mat Garland" \
      com.kromosome.email="matg@kromosome.net" \
      com.kromosome.vendor="Centos" \
      com.kromosome.license="GPLv2" \
      com.kromosome.version="7.2.1511" \
      com.kromosome.php_version="5.4.16" \
      com.kromosome.description="PHP 5.4.16 HOSTED IN CENTOS 7.2.1511"
# UPDATE THE RPM PACKAGE MANAGEMENT DATABASE
ADD ./docker_deps/yum.conf /etc/yum.conf
RUN yum -y update && yum clean all && yum -y install yum-plugin-ovl &> /dev/null | tee echo "UPDATING YUM & INSTALLING THE OVL PLUGIN"
RUN yum clean all && yum -y install wget sudo
RUN yum -y clean all && yum -y install php httpd httpd-suexec bzip2-libs libcom_err glibc libdb ca-certificates openssl-libs openssl-devel libcrypto10 php-gmp krb5-libs glibc-core glibc-pthread libxml2 zlib php-cli php-common php-mbstring php-mcrypt php-bcmath mhash which vim git git-core bash-completion zip unzip curl libevent libevent-devel memcached php-pecl-memcache php-pecl-memcached libssl-dev php-pear php-ftp php-fileinfo php-ereg subversion libicu php_pdo pdo_mysql soap sockets libxslt libcurl php-xml php-gd php-imap php-ldap systemd-sysv php-iconv php-phar php-sockets php-intl php-tokenizer netstat
# ADD USER www-data
RUN useradd -ms /bin/bash www-data
# ADD XDEBUG CONFIG FOR REMOTE DEBUGGING
ADD ./docker_deps/xdebug.ini /usr/local/etc/php/xdebug.ini
# SHORT OPEN TAGS FIX - SYMFONY REQUIREMENTS
ADD ./docker_deps/custom-php.ini /usr/local/etc/php/php.ini
# CUSTOMISED MEMCACHED CONFIGURATION
ADD ./docker_deps/memcached.ini /etc/sysconfig/memcached
# CLEAN UP THE YUM CACHE
RUN rm -rf /var/cache/yum
# INSTALL COMPOSER
ENV COMPOSER_HOME /var/www/.composer
RUN mkdir -p $COMPOSER_HOME/cache
RUN mkdir -p /bin
WORKDIR /bin
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php --install-dir=/bin --filename=composer
RUN php -r "unlink('composer-setup.php');"
VOLUME ["$COMPOSER_HOME"]
RUN mkdir -p $COMPOSER_HOME/cache
WORKDIR /var/www/html
# INSTALL SYMFONY FRAMEWORK
RUN sudo mkdir -p /usr/local/bin && sudo curl -LsS https://symfony.com/installer -o /usr/local/bin/symfony
RUN sudo chmod a+x /usr/local/bin/symfony
# INITIALISE SYMFONY 2.8 PROJECT IN html DIRECTORY
CMD ["symfony new dev.az.com 3.8"]
# RUN COMPOSER INSTALL TO INSTALL ALL DEPENDENCIES
RUN composer install --no-dev --prefer-dist --optimize-autoloader --no-scripts
# EXPOSE PORTS
EXPOSE 443
EXPOSE 80
# TO ENABLE systemd THIS SCRIPT MUST BE EXECUTED
# SEE: https://stackoverflow.com/questions/28495341/start-a-service-in-docker-container-failed-with-error-failed-to-get-d-bus-conne/42534263#42534263
# SEE: https://github.com/docker-library/docs/tree/master/centos#systemd-integration
ENV container docker
RUN (cd /lib/systemd/system/sysinit.target.wants/; for i in *; do [ $i == \
systemd-tmpfiles-setup.service ] || rm -f $i; done); \
rm -f /lib/systemd/system/multi-user.target.wants/*;\
rm -f /etc/systemd/system/*.wants/*;\
rm -f /lib/systemd/system/local-fs.target.wants/*; \
rm -f /lib/systemd/system/sockets.target.wants/*udev*; \
rm -f /lib/systemd/system/sockets.target.wants/*initctl*; \
rm -f /lib/systemd/system/basic.target.wants/*;\
rm -f /lib/systemd/system/anaconda.target.wants/*;
VOLUME [ "/sys/fs/cgroup" ]
# CHANGE OWNERSHIP OF WWW DIR TO www-data AND SWITCH TO USER www-data 
RUN chown -R www-data:www-data /var/www/
USER www-data
# EXEC /USR/BIN/INIT IS REQUIRED:
# SEE: https://github.com/docker-library/docs/tree/master/centos#systemd-integration
CMD ["/usr/sbin/init"]


# DUE TO SYSTEMD'S OBSCURITIES ON CENTOS, THIS CONTAINER MUST BE STARTED WITH:
# docker run -it -v /sys/fs/cgroup:/sys/fs/cgroup:ro -v /tmp/$(mktemp -d):/run -p 81:80 centos7/php5-4-16 /bin/bash
